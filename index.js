const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

const loans = [
    {
        laiId: '123',
        timeToDisplayLoan: "2021-06-10T06:00:00.804Z",
        timeToFundStart: "2021-06-11T06:11:26.804Z",
    },
    {
        laiId: '456',
        timeToDisplayLoan: "2021-06-10T06:30:00.804Z",
        timeToFundStart: "2021-06-12T15:27:46.904Z",
    },
    {
        laiId: '789',
        timeToDisplayLoan: "2021-06-10T07:06:00.804Z",
        timeToFundStart: "2021-06-13T06:11:00.804Z",
    }
];

const getTimeRemaining = endtime =>{
    const total = Date.parse(endtime) - Date.parse(new Date());
    const seconds = Math.floor( (total/1000) % 60 );
    const minutes = Math.floor( (total/1000/60) % 60 );
    const hours = Math.floor( (total/(1000*60*60)) % 24 );
    const days = Math.floor( total/(1000*60*60*24) );
  
    return {
      total,
      days,
      hours,
      minutes,
      seconds
    };
}

loans.forEach(loan => {
    const timerFrequency = 1000;
    const timer = global.setInterval(function() {
        const timeToDisplayLoan = getTimeRemaining(loan.timeToDisplayLoan);
        const timeToFundStart = getTimeRemaining(loan.timeToFundStart);
        if (timeToDisplayLoan.total <= 0) {
            console.log(`${loan.laiId} is open for funding in ${timeToFundStart.total}`);
            io.sockets.emit('countdown', {
                ...loan,
                ...timeToFundStart
            });
        }
        if (timeToFundStart.total <= 0) {
            clearTimeout(timer);
        }
    }, timerFrequency);
});

server.listen(4000, () => {
  console.log('listening on *:4000');
});
